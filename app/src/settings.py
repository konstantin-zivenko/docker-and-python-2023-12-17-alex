import dataclasses
import os

from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.


@dataclasses.dataclass
class Settings:
    BOT_TOKEN = os.environ.get("BOT_TOKEN", "some")
